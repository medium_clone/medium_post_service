package service

import (
	"context"

	pb "gitlab.com/medium_clone/medium_post_service/genproto/post_service"
	"gitlab.com/medium_clone/medium_post_service/storage"
	"gitlab.com/medium_clone/medium_post_service/storage/repo"
)

type LikeService struct {
	pb.UnimplementedLikeServiceServer
	storage storage.StorageI
}

func NewLikeService(strg storage.StorageI) *LikeService {
	return &LikeService{
		UnimplementedLikeServiceServer: pb.UnimplementedLikeServiceServer{},
		storage:                        strg,
	}
}

func (l *LikeService) CreateOrUpdate(ctx context.Context, req *pb.CreateOrUpdateLikeRequest) (*pb.Pustoy, error) {
	err := l.storage.Like().CreateOrUpdate(&repo.Like{
		UserId: int(req.UserId),
		PostId: int(req.PostId),
		Status: req.Status,
	})
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func (l *LikeService) Get(ctx context.Context, req *pb.GetLike) (*pb.CreateOrUpdateLikeRequest, error) {
	resp, err := l.storage.Like().Get(int(req.UserId), int(req.PostId))
	if err != nil {
		return nil, err
	}
	return &pb.CreateOrUpdateLikeRequest{
		Id:     int64(resp.ID),
		PostId: int64(resp.PostId),
		UserId: int64(resp.UserId),
		Status: resp.Status,
	}, nil
}

func (l *LikeService) GetLikesDislikesCount(ctx context.Context, id *pb.GetAllRequest) (*pb.GetAllResponse, error) {
	resp, err := l.storage.Like().GetLikesDislikesCount(int(id.PostId))
	if err != nil {
		return nil, err
	}
	return &pb.GetAllResponse{
		LikesCount:    int64(resp.LikesCount),
		DislikesCount: int64(resp.DislikesCount),
	}, nil
}
