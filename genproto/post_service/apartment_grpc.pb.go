// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package post_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ApartmentServiceClient is the client API for ApartmentService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ApartmentServiceClient interface {
	CreateApartment(ctx context.Context, in *GenerateApartment, opts ...grpc.CallOption) (*Apartment, error)
	GetApartment(ctx context.Context, in *IdByRequestApartment, opts ...grpc.CallOption) (*Apartment, error)
	GetAllApartmentRequest(ctx context.Context, in *GetApartmentRequest, opts ...grpc.CallOption) (*GetApartmentResponse, error)
	UpdateApartment(ctx context.Context, in *ChangeApartment, opts ...grpc.CallOption) (*Apartment, error)
	DeleteApartment(ctx context.Context, in *IdByRequestApartment, opts ...grpc.CallOption) (*EmptyApartment, error)
	ViewInc(ctx context.Context, in *IdByRequestApartment, opts ...grpc.CallOption) (*EmptyApartment, error)
}

type apartmentServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewApartmentServiceClient(cc grpc.ClientConnInterface) ApartmentServiceClient {
	return &apartmentServiceClient{cc}
}

func (c *apartmentServiceClient) CreateApartment(ctx context.Context, in *GenerateApartment, opts ...grpc.CallOption) (*Apartment, error) {
	out := new(Apartment)
	err := c.cc.Invoke(ctx, "/genproto.ApartmentService/CreateApartment", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *apartmentServiceClient) GetApartment(ctx context.Context, in *IdByRequestApartment, opts ...grpc.CallOption) (*Apartment, error) {
	out := new(Apartment)
	err := c.cc.Invoke(ctx, "/genproto.ApartmentService/GetApartment", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *apartmentServiceClient) GetAllApartmentRequest(ctx context.Context, in *GetApartmentRequest, opts ...grpc.CallOption) (*GetApartmentResponse, error) {
	out := new(GetApartmentResponse)
	err := c.cc.Invoke(ctx, "/genproto.ApartmentService/GetAllApartmentRequest", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *apartmentServiceClient) UpdateApartment(ctx context.Context, in *ChangeApartment, opts ...grpc.CallOption) (*Apartment, error) {
	out := new(Apartment)
	err := c.cc.Invoke(ctx, "/genproto.ApartmentService/UpdateApartment", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *apartmentServiceClient) DeleteApartment(ctx context.Context, in *IdByRequestApartment, opts ...grpc.CallOption) (*EmptyApartment, error) {
	out := new(EmptyApartment)
	err := c.cc.Invoke(ctx, "/genproto.ApartmentService/DeleteApartment", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *apartmentServiceClient) ViewInc(ctx context.Context, in *IdByRequestApartment, opts ...grpc.CallOption) (*EmptyApartment, error) {
	out := new(EmptyApartment)
	err := c.cc.Invoke(ctx, "/genproto.ApartmentService/ViewInc", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ApartmentServiceServer is the server API for ApartmentService service.
// All implementations must embed UnimplementedApartmentServiceServer
// for forward compatibility
type ApartmentServiceServer interface {
	CreateApartment(context.Context, *GenerateApartment) (*Apartment, error)
	GetApartment(context.Context, *IdByRequestApartment) (*Apartment, error)
	GetAllApartmentRequest(context.Context, *GetApartmentRequest) (*GetApartmentResponse, error)
	UpdateApartment(context.Context, *ChangeApartment) (*Apartment, error)
	DeleteApartment(context.Context, *IdByRequestApartment) (*EmptyApartment, error)
	ViewInc(context.Context, *IdByRequestApartment) (*EmptyApartment, error)
	mustEmbedUnimplementedApartmentServiceServer()
}

// UnimplementedApartmentServiceServer must be embedded to have forward compatible implementations.
type UnimplementedApartmentServiceServer struct {
}

func (UnimplementedApartmentServiceServer) CreateApartment(context.Context, *GenerateApartment) (*Apartment, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateApartment not implemented")
}
func (UnimplementedApartmentServiceServer) GetApartment(context.Context, *IdByRequestApartment) (*Apartment, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetApartment not implemented")
}
func (UnimplementedApartmentServiceServer) GetAllApartmentRequest(context.Context, *GetApartmentRequest) (*GetApartmentResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAllApartmentRequest not implemented")
}
func (UnimplementedApartmentServiceServer) UpdateApartment(context.Context, *ChangeApartment) (*Apartment, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateApartment not implemented")
}
func (UnimplementedApartmentServiceServer) DeleteApartment(context.Context, *IdByRequestApartment) (*EmptyApartment, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteApartment not implemented")
}
func (UnimplementedApartmentServiceServer) ViewInc(context.Context, *IdByRequestApartment) (*EmptyApartment, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ViewInc not implemented")
}
func (UnimplementedApartmentServiceServer) mustEmbedUnimplementedApartmentServiceServer() {}

// UnsafeApartmentServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ApartmentServiceServer will
// result in compilation errors.
type UnsafeApartmentServiceServer interface {
	mustEmbedUnimplementedApartmentServiceServer()
}

func RegisterApartmentServiceServer(s grpc.ServiceRegistrar, srv ApartmentServiceServer) {
	s.RegisterService(&ApartmentService_ServiceDesc, srv)
}

func _ApartmentService_CreateApartment_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GenerateApartment)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApartmentServiceServer).CreateApartment(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.ApartmentService/CreateApartment",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApartmentServiceServer).CreateApartment(ctx, req.(*GenerateApartment))
	}
	return interceptor(ctx, in, info, handler)
}

func _ApartmentService_GetApartment_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdByRequestApartment)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApartmentServiceServer).GetApartment(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.ApartmentService/GetApartment",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApartmentServiceServer).GetApartment(ctx, req.(*IdByRequestApartment))
	}
	return interceptor(ctx, in, info, handler)
}

func _ApartmentService_GetAllApartmentRequest_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetApartmentRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApartmentServiceServer).GetAllApartmentRequest(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.ApartmentService/GetAllApartmentRequest",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApartmentServiceServer).GetAllApartmentRequest(ctx, req.(*GetApartmentRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ApartmentService_UpdateApartment_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ChangeApartment)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApartmentServiceServer).UpdateApartment(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.ApartmentService/UpdateApartment",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApartmentServiceServer).UpdateApartment(ctx, req.(*ChangeApartment))
	}
	return interceptor(ctx, in, info, handler)
}

func _ApartmentService_DeleteApartment_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdByRequestApartment)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApartmentServiceServer).DeleteApartment(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.ApartmentService/DeleteApartment",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApartmentServiceServer).DeleteApartment(ctx, req.(*IdByRequestApartment))
	}
	return interceptor(ctx, in, info, handler)
}

func _ApartmentService_ViewInc_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdByRequestApartment)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApartmentServiceServer).ViewInc(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/genproto.ApartmentService/ViewInc",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApartmentServiceServer).ViewInc(ctx, req.(*IdByRequestApartment))
	}
	return interceptor(ctx, in, info, handler)
}

// ApartmentService_ServiceDesc is the grpc.ServiceDesc for ApartmentService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var ApartmentService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "genproto.ApartmentService",
	HandlerType: (*ApartmentServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateApartment",
			Handler:    _ApartmentService_CreateApartment_Handler,
		},
		{
			MethodName: "GetApartment",
			Handler:    _ApartmentService_GetApartment_Handler,
		},
		{
			MethodName: "GetAllApartmentRequest",
			Handler:    _ApartmentService_GetAllApartmentRequest_Handler,
		},
		{
			MethodName: "UpdateApartment",
			Handler:    _ApartmentService_UpdateApartment_Handler,
		},
		{
			MethodName: "DeleteApartment",
			Handler:    _ApartmentService_DeleteApartment_Handler,
		},
		{
			MethodName: "ViewInc",
			Handler:    _ApartmentService_ViewInc_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "apartment.proto",
}
