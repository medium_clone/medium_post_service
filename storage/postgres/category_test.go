package postgres_test

import (
	"testing"
	"time"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/medium_clone/medium_post_service/storage/repo"
)

func createCategory(t *testing.T) *repo.Category {
	c, err := strg.Category().Create(&repo.Category{
		Id:        1,
		Title:     faker.Sentence(),
		CreatedAt: time.Now(),
	})
	require.NotEmpty(t, c)
	require.NoError(t, err)

	return c
}

func deleteCategory(t *testing.T, id int) {
	err := strg.Category().Delete(id)
	require.NoError(t, err)
}

func TestCreateCategory(t *testing.T) {
	category := createCategory(t)
	deleteCategory(t, category.Id)
}

func TestGetCategory(t *testing.T) {
	category := createCategory(t)
	ctg, err := strg.Category().Get(category.Id)
	require.NoError(t, err)
	require.NotEmpty(t, ctg)
	deleteCategory(t, ctg.Id)
}

func TestGetAllCategory(t *testing.T) {
	num, err := faker.RandomInt(100)
	require.NoError(t, err)
	categories, err := strg.Category().GetAll(repo.GetCategoryQuery{
		Limit: num[0],
		Page:  1,
	})
	require.NoError(t, err)
	require.NotEmpty(t, categories)
}

func TestUpdateCategory(t *testing.T) {
	category := createCategory(t)
	require.NotEmpty(t, category)
	newCategory, err := strg.Category().Update(&repo.Category{
		Id:    category.Id,
		Title: faker.Sentence(),
	})
	require.NoError(t, err)
	require.NotEqual(t, category, newCategory)
	require.NotEmpty(t, newCategory)

	deleteCategory(t, category.Id)
}

func TestDeleteCategory(t *testing.T) {
	category := createCategory(t)
	deleteCategory(t, category.Id)
}
