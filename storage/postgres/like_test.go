package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/medium_clone/medium_post_service/storage/repo"
)

func TestCreateOrUpdate(t *testing.T) {
	post := createPost(t)
	num, err := faker.RandomInt(100)
	require.NoError(t, err)
	err = strg.Like().CreateOrUpdate(&repo.Like{
		PostId: int(post.Id),
		UserId: num[0],
		Status: num[0]%2 != 0,
	})
	require.NoError(t, err)
	deletePost(t, int(post.Id))
}

func TestGet(t *testing.T) {
	post := createPost(t)
	num, err := faker.RandomInt(100)
	require.NoError(t, err)
	ctg, _ := strg.Like().Get(num[0], int(post.Id))
	require.NotEmpty(t, ctg)
	deletePost(t, int(post.Id))
}

func TestGetLikesDislikesCount(t *testing.T) {
	post := createPost(t)
	_, err := strg.Like().GetLikesDislikesCount(int(post.Id))
	require.NoError(t, err)
	deletePost(t, int(post.Id))
}
