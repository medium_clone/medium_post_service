package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/medium_clone/medium_post_service/storage/repo"
)

func createPost(t *testing.T) *repo.Post {
	category := createCategory(t)
	num, err := faker.RandomInt(100)
	require.NoError(t, err)
	p, err := strg.Post().Create(&repo.Post{
		Title:       faker.Sentence(),
		Description: faker.Sentence(),
		ImageUrl:    faker.URL(),
		CategoryId:  int64(category.Id),
		UserId:      int64(num[0]),
	})
	require.NotEmpty(t, p)
	require.NoError(t, err)
	return p
}

func deletePost(t *testing.T, id int) {
	err := strg.Post().Delete(id)
	require.NoError(t, err)
}

func TestCreatePost(t *testing.T) {
	post := createPost(t)
	deletePost(t, int(post.Id))
}

func TestGetPost(t *testing.T) {
	post := createPost(t)
	ctg, err := strg.Post().Get(int(post.Id))
	require.NoError(t, err)
	require.NotEmpty(t, ctg)
	deletePost(t, int(ctg.Id))
}

func TestGetAllPost(t *testing.T) {
	num, err := faker.RandomInt(100)
	require.NoError(t, err)
	categories, err := strg.Post().GetAll(repo.GetPostQuery{
		Limit: int32(num[0]),
		Page:  1,
	})
	require.NoError(t, err)
	require.NotEmpty(t, categories)
}

func TestUpdatePost(t *testing.T) {
	post := createPost(t)
	require.NotEmpty(t, post)
	newPost, err := strg.Post().Update(&repo.ChangePost{
		Id:          post.Id,
		UserId:      post.UserId,
		Title:       faker.Sentence(),
		Description: faker.Sentence(),
		ImageUrl:    faker.URL(),
	})
	require.NoError(t, err)
	require.NotEqual(t, post, newPost)
	require.NotEmpty(t, newPost)

	deletePost(t, int(post.Id))
}

func TestDeletePost(t *testing.T) {
	post := createPost(t)
	deletePost(t, int(post.Id))
}
