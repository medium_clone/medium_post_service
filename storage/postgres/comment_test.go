package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/medium_clone/medium_post_service/storage/repo"
)

func createComment(t *testing.T) *repo.Comment {
	post := createPost(t)
	num, err := faker.RandomInt(100)
	require.NoError(t, err)
	c, err := strg.Comment().Create(&repo.Comment{
		PostId:      int(post.Id),
		UserId:      num[0],
		Description: faker.Sentence(),
	})
	require.NotEmpty(t, c)
	require.NoError(t, err)

	return c
}

func deleteComment(t *testing.T, id int) {
	err := strg.Comment().Delete(id)
	require.NoError(t, err)
}

func TestCreateComment(t *testing.T) {
	comment := createComment(t)
	deleteComment(t, comment.Id)
}

func TestGetComment(t *testing.T) {
	comment := createComment(t)
	ctg, err := strg.Comment().Get(comment.Id)
	require.NoError(t, err)
	require.NotEmpty(t, ctg)
	deleteComment(t, ctg.Id)
}

func TestGetAllComment(t *testing.T) {
	num, err := faker.RandomInt(100)
	require.NoError(t, err)
	categories, err := strg.Comment().GetAll(repo.GetCommentQuery{
		Limit: num[0],
		Page:  1,
	})
	require.NoError(t, err)
	require.NotEmpty(t, categories)
}

func TestUpdateComment(t *testing.T) {
	comment := createComment(t)
	require.NotEmpty(t, comment)
	newComment, err := strg.Comment().Update(&repo.Comment{
		Id:          comment.Id,
		Description: faker.Sentence(),
	})
	require.NoError(t, err)
	require.NotEqual(t, comment, newComment)
	require.NotEmpty(t, newComment)

	deleteComment(t, comment.Id)
}

func TestDeleteComment(t *testing.T) {
	comment := createComment(t)
	deleteComment(t, comment.Id)
}
