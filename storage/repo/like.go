package repo

type Like struct {
	ID     int
	PostId int
	UserId int
	Status bool
}

type LikesDislikesCountsResult struct {
	LikesCount    int
	DislikesCount int
}

type LikeStorageI interface {
	CreateOrUpdate(l *Like) error
	Get(UserId, PostId int) (*Like, error)
	GetLikesDislikesCount(PostId int) (*LikesDislikesCountsResult, error)
}
