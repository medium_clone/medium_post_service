package main

import (
	"fmt"
	"log"
	"net"

	_ "github.com/lib/pq"

	"github.com/jmoiron/sqlx"
	pb "gitlab.com/medium_clone/medium_post_service/genproto/post_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/medium_clone/medium_post_service/config"
	"gitlab.com/medium_clone/medium_post_service/service"
	"gitlab.com/medium_clone/medium_post_service/storage"
)

func main() {
	cfg := config.Load(".")

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}

	strg := storage.NewStoragePg(psqlConn)

	postService := service.NewPostService(strg)
	categoryService := service.NewCategoryService(strg)
	commentService := service.NewCommentService(strg)
	likeService := service.NewLikeService(strg)

	lis, err := net.Listen("tcp", cfg.GrpcPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	reflection.Register(s)

	pb.RegisterPostServiceServer(s, postService)
	pb.RegisterCategoryServiceServer(s, categoryService)
	pb.RegisterCommentServiceServer(s, commentService)
	pb.RegisterLikeServiceServer(s, likeService)

	log.Println("Grpc server started in port ", cfg.GrpcPort)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Error while listening: %v", err)
	}
}
